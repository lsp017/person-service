package com.cts.personservice.resource;

import com.cts.personservice.model.PersonBase;
import com.cts.personservice.model.PhoneNumber;
import com.cts.personservice.service.PersonService;
import com.cts.personservice.service.PhoneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/persons/{personId}/phones")
@Api("Set of endpoints for Creating, Retrieving, Updating and Deleting Phones.")
public class PhoneResource {

    private static final Logger LOG = LoggerFactory.getLogger(PhoneResource.class);
    private final PhoneService phoneService;
    public PhoneResource(PhoneService phoneService) {
        this.phoneService = phoneService;
    }


    @ApiOperation("Update a person phone. 404 if the person's identifier is not found.")
    @PutMapping(value = "/phone")
    public ResponseEntity<Void> addPhoneNumber(@PathVariable String personId,
                                             @RequestBody PhoneNumber phoneNumber,
                                             @RequestHeader(value = "If-Match") String ifMatch,
                                             @RequestHeader(value = "X-Tenant-Id",required = false) String tenantId) {
        String version = phoneService.addPhoneNumber(personId, ifMatch, phoneNumber);
        LOG.info("Person Phone update is successful, personId={}", personId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .eTag(version)
                .build();
    }

    @ApiOperation("Delete a person Phone. 404 if the persons's identifier is not found.")
    @DeleteMapping(value = "/{phoneId}")
    public ResponseEntity<Void> deletePhone(@PathVariable String personId,
                                            @PathVariable String phoneId,
                                             @RequestHeader(value = "X-Tenant-Id",required = false) String tenantId) {
        phoneService.deletePhone(personId,phoneId);
        LOG.info("Phone delete is successful, personId={}", personId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .build();
    }

}
