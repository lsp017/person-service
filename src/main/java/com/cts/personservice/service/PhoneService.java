package com.cts.personservice.service;

import com.cts.personservice.entity.PersonEntity;
import com.cts.personservice.entity.PhoneNumberEntity;
import com.cts.personservice.exception.RecordNotFoundException;
import com.cts.personservice.model.PhoneNumber;
import com.cts.personservice.repository.PersonRepository;
import com.cts.personservice.service.converter.PersonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.cts.personservice.util.EncodingUtils.decodeVersion;
import static com.cts.personservice.util.EncodingUtils.encodeVersion;

@Service
public class PhoneService {

    private static final Logger LOG = LoggerFactory.getLogger(PersonService.class);

    private final PersonConverter personConverter;
    private final PersonRepository personRepository;

    public PhoneService(PersonConverter personConverter, PersonRepository personRepository) {
        this.personConverter = personConverter;
        this.personRepository = personRepository;
    }
    //Add phone number
    public String addPhoneNumber(String personId, String ifMatch, PhoneNumber phoneNumber) {
        LOG.info("Updating phone number with personId={}, version={}", personId, decodeVersion(ifMatch));
        Optional<PersonEntity> personEntity = personRepository.findById(personId);
        personEntity.orElseThrow(() -> new RecordNotFoundException("Person not found with personId=" + personId));
        PhoneNumberEntity phoneNumberEntity = personConverter.phoneModelToEntity(phoneNumber);
        if(Objects.isNull(personEntity.get().getPhones())) {
            personEntity.get().setPhones(new ArrayList<>());
        }
        personEntity.get().getPhones().add(phoneNumberEntity);
        personRepository.save(personEntity.get());

        return encodeVersion(personEntity.get().getVersion());
    }
    //Delete Phone number
    public void deletePhone(String personId, String phoneId) {
        LOG.info("Deleting Phone with personId={}");
        Optional<PersonEntity> personEntity = personRepository.findById(personId);
        personEntity.orElseThrow(() -> new RecordNotFoundException("Person not found with personId,phoneId=" + personId + "," + phoneId));
        if(!Objects.isNull(personEntity.get().getPhones())) {
            personEntity.get().getPhones().removeIf(item -> item.getPhoneId().equals(phoneId));
       }
        personRepository.save(personEntity.get());
    }



}
